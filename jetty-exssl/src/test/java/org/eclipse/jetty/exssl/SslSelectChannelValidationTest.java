package org.eclipse.jetty.exssl;

import org.eclipse.jetty.client.HttpClient;

public class SslSelectChannelValidationTest extends SslValidationTestBase
{
    static
    {
        __klass = SslSelectChannelConnector.class;
        __konnector = HttpClient.CONNECTOR_SELECT_CHANNEL;
    }
}
