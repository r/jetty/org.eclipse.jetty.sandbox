package org.eclipse.jetty.nosql.ehcache;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;

import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.nosql.NoSqlSession;
import org.eclipse.jetty.nosql.NoSqlSessionManager;
import org.eclipse.jetty.util.ajax.JSON;


public class EhcacheSessionManager extends NoSqlSessionManager
{
    private HttpClient _client = new HttpClient();
    private URI _cachingServerUri;
    private String _cache;
    
    public EhcacheSessionManager(URI cachingServerUri, String cache)
    {
        _cachingServerUri = cachingServerUri;
        _cache = cache;
        
        try
        {
            _client.start();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        checkOrCreateCache();
    }
    
    private void checkOrCreateCache()
    {
        EhcacheExchange saveExchange = new EhcacheExchange();
        saveExchange.setMethod("PUT");
        
        URI saveURI = _cachingServerUri.resolve(_cache);
        
        
        saveExchange.setURL(saveURI.toString());
        
        try
        {
            _client.send(saveExchange);
           
            saveExchange.waitForDone();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    protected NoSqlSession loadSession(String clusterId)
    {
        EhcacheExchange loadExchange = new EhcacheExchange();
        loadExchange.setMethod("GET");
        URI loadURI = _cachingServerUri.resolve(_cache).resolve(clusterId);
        loadExchange.setURL(loadURI.toString());
        
        try
        {
            _client.send(loadExchange);
            loadExchange.waitForDone();
            
            System.out.println("Session: " + loadExchange.getResponseContent());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        
        System.out.println("Load URI: " + loadURI);
        
        return null;    
    }

    @Override
    protected Object save(NoSqlSession session, Object version, boolean activateAfterSave)
    {
        EhcacheExchange saveExchange = new EhcacheExchange();
        saveExchange.setMethod("PUT");
        saveExchange.setRequestContentSource(new ByteArrayInputStream(JSON.toString(new JsonSessionData(session)).getBytes()));
        URI saveURI = _cachingServerUri.resolve(_cache).resolve(session.getClusterId());
        saveExchange.setURL(saveURI.toString());
        
        try
        {
            _client.send(saveExchange);
            saveExchange.waitForDone();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        
        System.out.println("Save URI: " + saveURI);
        
        return session.getVersion();
    }

    @Override
    protected Object refresh(NoSqlSession session, Object version)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected boolean remove(NoSqlSession session)
    {
        // TODO Auto-generated method stub
        return false;
    }

}
