package org.eclipse.jetty.nosql.ehcache;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.jetty.nosql.NoSqlSession;

public class JsonSessionData
{
    public String clusterId;
    public String nodeId;
    public Long creationTime;
    public Long accessed;
    public Integer requests;
    public Integer attributes;
    public Object version;
    public Map<String, Object> attrs = new HashMap<String,Object>();

    public JsonSessionData(NoSqlSession session)
    {
        clusterId = session.getClusterId();
        nodeId = session.getNodeId();
        creationTime = session.getCreationTime();
        accessed = session.getAccessed();
        requests = session.getRequests();
        attributes = session.getAttributes();
        version = session.getVersion();
        
        Enumeration<String> e = session.getAttributeNames();
        while (e.hasMoreElements())
        {
            String key = e.nextElement();
            attrs.put(key,session.getAttribute(key));
        }
    }
}