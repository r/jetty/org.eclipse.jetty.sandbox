package org.eclipse.jetty.exssl;

import java.io.File;
import java.security.cert.CertificateException;

import org.eclipse.jetty.io.SslContextFactory;
import org.eclipse.jetty.toolchain.test.MavenTestingUtils;
import org.junit.Test;

public class CertificateValidatorTest
{
    protected void doTest(String keystore) throws Exception
    {
        String keypath = MavenTestingUtils.getTestResourceFile(keystore).getAbsolutePath();
        String trustpath = new File(System.getProperty("java.home"),"./lib/security/cacerts").getAbsolutePath();
        String crlpath = MavenTestingUtils.getTestResourceFile("crlfile.pem").getAbsolutePath();

        SslContextFactory factory = new SslContextFactory(null);
        factory.setValidateCerts(true);
        factory.setKeystore(keypath);
        factory.setKeystorePassword("webtide");
        factory.setKeyManagerPassword("webtide");
        factory.setTruststore(trustpath);
        factory.setTruststorePassword("changeit");
        factory.setCrlPath(crlpath);
        
        factory.start();
    }
    
    @Test
    public void validCertificateTest() throws Exception
    {
        doTest("jetty-valid.keystore"); // certificate is valid until Jan 1, 2050
    }
    
    @Test(expected = CertificateException.class)
    public void revokedCertificateTest() throws Exception
    {
        doTest("jetty-revoked.keystore"); // certificate is valid until Jan 1, 2050
    }

    @Test(expected = CertificateException.class)
    public void notvalidCertificateTest() throws Exception
    {
        doTest("jetty-notvalid.keystore"); // certificate is valid from Jan 1, 2049
    }

    @Test(expected = CertificateException.class)
    public void expiredCertificateTest() throws Exception
    {
        doTest("jetty-expired.keystore"); // certificate is valid until Dec 31, 2000
    }
}
