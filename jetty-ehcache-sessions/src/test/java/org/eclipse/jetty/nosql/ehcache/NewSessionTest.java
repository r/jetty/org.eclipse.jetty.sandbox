package org.eclipse.jetty.nosql.ehcache;

import org.eclipse.jetty.server.session.AbstractNewSessionTest;
import org.eclipse.jetty.server.session.AbstractTestServer;

public class NewSessionTest extends AbstractNewSessionTest
{

    @Override
    public AbstractTestServer createServer(int port, int max, int scavenge)
    {
        return new TestServer(0);
    }

}
