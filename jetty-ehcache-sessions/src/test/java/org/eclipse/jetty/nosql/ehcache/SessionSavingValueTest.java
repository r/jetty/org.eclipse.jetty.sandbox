package org.eclipse.jetty.nosql.ehcache;

import org.eclipse.jetty.server.session.AbstractSessionValueSavingTest;
import org.eclipse.jetty.server.session.AbstractTestServer;

public class SessionSavingValueTest extends AbstractSessionValueSavingTest
{

    @Override
    public AbstractTestServer createServer(int port, int max, int scavenge)
    {
        return new TestServer(0);
    }

}
