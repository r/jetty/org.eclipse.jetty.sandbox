package org.eclipse.jetty.nosql.ehcache;

import java.net.URI;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.SessionIdManager;
import org.eclipse.jetty.server.SessionManager;
import org.eclipse.jetty.server.nio.SelectChannelConnector;
import org.eclipse.jetty.server.session.AbstractTestServer;
import org.eclipse.jetty.server.session.SessionHandler;
import org.eclipse.jetty.toolchain.test.MavenTestingUtils;
import org.eclipse.jetty.webapp.WebAppContext;

public class EhcacheServer
{
    private Server _server;
    private int _port;
    private String _host;
    private String _path;

    public EhcacheServer()
    {

    }
    
    public void start() throws Exception
    {
        _server = new Server();

        Connector connector = new SelectChannelConnector();
        _server.setConnectors(new Connector[] { connector });

        String war = MavenTestingUtils.getTargetFile("ehcache-server.war").getAbsolutePath();

        WebAppContext webapp = new WebAppContext();
        webapp.setWar(war);
        webapp.setParentLoaderPriority(true);
        _server.setHandler(webapp);

        _server.start();
        
        _port = connector.getLocalPort();
        _host = connector.getHost();
        if (_host == null)
        {
            _host = "localhost";
        }
        _path = webapp.getContextPath();
    }
    
    
    public void stop() throws Exception
    {
        _server.stop();
        
        _server = null;
    }
    
    public URI getBaseURI() throws Exception
    {
        return new URI("http://" + _host + ":" + _port + _path + "/rest/");
        
    }
    
  
}
