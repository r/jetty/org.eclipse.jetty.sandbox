package org.eclipse.jetty.nosql.ehcache;

import org.eclipse.jetty.server.SessionIdManager;
import org.eclipse.jetty.server.SessionManager;
import org.eclipse.jetty.server.session.AbstractTestServer;
import org.eclipse.jetty.server.session.SessionHandler;

public class TestServer extends AbstractTestServer
{
    EhcacheServer _server = new EhcacheServer();
    
    public TestServer(int port)
    {
        super(port);
        
        try
        {
            _server.start();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    @Override
    public SessionIdManager newSessionIdManager()
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public SessionManager newSessionManager()
    {   
        try
        {
            return new EhcacheSessionManager(_server.getBaseURI(), "HttpSessions");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public SessionHandler newSessionHandler(SessionManager sessionManager)
    {
        return new SessionHandler(sessionManager);
    }

}
