package org.eclipse.jetty.nosql.ehcache;

import org.eclipse.jetty.server.session.AbstractLastAccessTimeTest;
import org.eclipse.jetty.server.session.AbstractTestServer;

public class LastAccessTimeTest extends AbstractLastAccessTimeTest
{

    @Override
    public AbstractTestServer createServer(int port, int max, int scavenge)
    {
        return new TestServer(0);
    }

}
