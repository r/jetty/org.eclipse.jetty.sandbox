package org.eclipse.jetty.exssl;

import org.eclipse.jetty.client.HttpClient;

public class SslSocketValidationTest extends SslValidationTestBase
{
    static
    {
        __klass = SslSocketConnector.class;
        __konnector = HttpClient.CONNECTOR_SOCKET;
    }
}
