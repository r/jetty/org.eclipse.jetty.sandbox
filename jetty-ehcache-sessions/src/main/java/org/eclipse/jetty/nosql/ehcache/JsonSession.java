package org.eclipse.jetty.nosql.ehcache;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.jetty.nosql.NoSqlSession;
import org.eclipse.jetty.util.ajax.JSON;
import org.eclipse.jetty.util.ajax.JSON.Output;
import org.eclipse.jetty.util.ajax.JSONPojoConvertor.Setter;

public class JsonSession implements JSON.Convertible
{
    public String clusterId;
    public String nodeId;
    public Long creationTime;
    public Long accessed;
    public Integer requests;
    public Integer attributes;
    public Object version = new Integer(0);
    public Map<String, Object> attrs = new HashMap<String,Object>();

    public JsonSession()
    {
        
    }
    
    public JsonSession(NoSqlSession session)
    {
        clusterId = session.getClusterId();
        nodeId = session.getNodeId();
        creationTime = session.getCreationTime();
        accessed = session.getAccessed();
        requests = session.getRequests();
        attributes = session.getAttributes();
        if ( session.getVersion() != null )
        {
            version = session.getVersion();
        }
        
        Enumeration<String> e = session.getAttributeNames();
        while (e.hasMoreElements())
        {
            String key = e.nextElement();
            attrs.put(key,session.getAttribute(key));
        }
    }

    public String getClusterId()
    {
        return clusterId;
    }

    public void setClusterId(String clusterId)
    {
        this.clusterId = clusterId;
    }

    public String getNodeId()
    {
        return nodeId;
    }

    public void setNodeId(String nodeId)
    {
        this.nodeId = nodeId;
    }

    public Long getCreationTime()
    {
        return creationTime;
    }

    public void setCreationTime(Long creationTime)
    {
        this.creationTime = creationTime;
    }

    public Long getAccessed()
    {
        return accessed;
    }

    public void setAccessed(Long accessed)
    {
        this.accessed = accessed;
    }

    public Integer getRequests()
    {
        return requests;
    }

    public void setRequests(Integer requests)
    {
        this.requests = requests;
    }

    public Integer getAttributes()
    {
        return attributes;
    }

    public void setAttributes(Integer attributes)
    {
        this.attributes = attributes;
    }

    public Object getVersion()
    {
        return version;
    }

    public void setVersion(Object version)
    {
        this.version = version;
    }
    
    public Map<String,Object> getAttrs()
    {
        return attrs;
    }
    
    @Override
    public void toJSON(Output out)
    {
        out.add("version", (Integer)getVersion() );
        out.add("clusterId", getClusterId() );
        out.add("creationTime", getCreationTime());
        out.add("accessed", getAccessed());
        out.add("requests", getRequests());
        out.add("attributes", getAttributes());
        
        out.add("attrs", attrs);
        
    }

    @Override
    public void fromJSON(Map object)
    {
        setRequests(Integer.valueOf("" + object.get("requests")));
        setVersion(object.get("version"));
        setAccessed((Long)object.get("accessed"));
        setCreationTime((Long)object.get("creationTime"));
        setAttributes(Integer.valueOf("" + object.get("attributes")));
        
        attrs = (HashMap<String,Object>)object.get("attrs");
    }
    
}